import os
import json

# config handling
reads = config["reads"]
frac_diff = config.get("frac_diff", 0.01)
min_size = config.get("min_size", 100)

opts = config.get("opts", {})

# dump config for reference
with open("config.json", "w") as f:
    json.dump(config, f, indent=4)



wildcard_constraints:
    n = "\d+"


rule all:
    input:
        ".completed_flag"


rule dup_reads:
    input:
        reads
    output:
        "duped.reads.fastq"
    script:
        "scripts/dup_reads.py"


rule overlap_reads:
    input:
        reads,
        rules.dup_reads.output[0]
    output:
        temp("overlap.paf")
    conda:
        "requirements.yml"
    params:
        opt = opts.get("overlap_reads", "")
    threads:
        8
    shell:
        "minimap2 -x ava-ont {params[opt]} -t {threads} {input} > {output}"


rule get_candidates:
    input:
        reads,
        rules.overlap_reads.output[0]
    output:
        "candidates.racon0.fasta",
        "clusters.json",
        "lengths.json",
        "candidate_info.json"
    params:
        frac_diff = frac_diff,
        min_size = min_size,
    conda:
        "requirements.yml"
    script:
        "scripts/get_candidates.py"


rule minimap2:
    input:
        "candidates.racon{n}.fasta",
        reads
    output:
        temp("alignments.racon{n}.paf")
    conda:
        "requirements.yml"
    params:
        opt = opts.get("minimap2", "")
    threads:
        8
    shell:
        "minimap2 -x map-ont {params[opt]} -t {threads} {input[0]} {input[1]} > {output[0]}"


rule racon:
    input:
        reads = reads,
        paf = lambda wcs: f"alignments.racon{int(wcs['n']) - 1}.paf",
        fasta =  lambda wcs: f"candidates.racon{int(wcs['n']) - 1}.fasta"
    output:
        "candidates.racon{n}.fasta"
    conda:
        "requirements.yml"
    threads:
        8
    params:
        opt = opts.get("racon", "")
    shell:
        "racon {params[opt]} -t {threads} {input[reads]} {input[paf]} {input[fasta]} > {output[0]}"


rule trim:
    input:
        "candidates.racon3.fasta"
    output:
        "final.fasta"
    conda:
        "requirements.yml"
    script:
        "scripts/trim_polished.py"


rule qc:
    input:
        "alignments.racon3.paf",
        "clusters.json"
    output:
        directory("histograms")
    params:
        min_size = min_size
    conda:
        "requirements.yml"
    script:
        "scripts/gen_histograms.py"
    

rule final:
    input:
        rules.trim.output[0]
    output:
        rules.all.input[0]
    shell:
        "touch {output}"
