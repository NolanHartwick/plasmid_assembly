from typing import Generator, Dict, Tuple, TextIO
import mappy


def load_fasta(fpath: str) -> Dict[str, str]:
    """ reads a fasta file at fpath and returns a dict mapping contig headers
        to sequence content
    """
    ret = {}
    with open(fpath) as f:
        for header, seq in parse_fasta(f):
            ret[header] = seq
    return ret


def parse_fasta(f: TextIO) -> Generator[Tuple[str, str], None, None]:
    """ Given a readable file like object containing fasta data, iterate
        over the fasta yielding a header, sequence pair for each entry

        >>> from io import StringIO
        >>> i = StringIO('>chr1\\nAAAAA\\n>chr2\\nCCCCC')
        >>> list(parse_fasta(i))
        [('chr1', 'AAAAA'), ('chr2', 'CCCCC')]
    """
    header = f.readline()[1:-1]
    if(header != ''):
        body = []
        for line in f:
            if(line[0] == '>'):
                yield (header, ''.join(body))
                header = line[1:-1]
                body = []
            else:
                body.append(line.rstrip())
        yield (header, ''.join(body))


def write_fasta(data: Dict[str, str], fp: TextIO):
    """ write a fasta file described by data to outpath.

        data
            A dict representing fasta sequence. {header: sequence}
        fp
            A file like object with "write"
        sep
            A string used to seperate the id from the info in the fasta
            headers. the written headers will be ">{id}" if info=="" else
            ">{id}{sep}{info}"
    """
    for header, seq in data.items():
        fp.write(">")
        fp.write(header)
        fp.write("\n")
        line_generator = (seq[i:i + 80] for i in range(0, len(seq), 80))
        for line in line_generator:
            fp.write(line)
            fp.write("\n")


def naive_trim(input_fasta: str, output_path: str):
    """ trims ends from input fasta to hopefully remove the intentional
        duplication
    """
    data = list(load_fasta(input_fasta).items())
    header = data[0][0]
    sequence = data[0][1]
    trimmed = sequence[int(0.25 * len(sequence)): int(0.75 * len(sequence))]
    with open(output_path, "w") as fout:
        write_fasta({header: trimmed}, fout)


def smart_trim(input_fasta: str, output_path: str):
    """ trim ends after using an aligner to identify circular break point
    """
    results = {}
    for header, sequence in load_fasta(input_fasta).items():
        seqlen = len(sequence)
        front = sequence[int(0.25 * seqlen): int(0.5 * seqlen)]
        end = sequence[int(0.6 * seqlen):]
        a = mappy.Aligner(seq=end)
        hits = list(a.map(front))
        if(len(hits) == 1):
            en_off = hits[0].r_st
            st_off = hits[0].q_st
            trimmed = sequence[int(0.25 * seqlen) + st_off: int(0.6 * seqlen) + en_off]
            results[header] = trimmed
        else:
            raise ValueError(f"unable to circularize: {header}")
    with open(output_path, "w") as fout:
        write_fasta(results, fout)


def snakemain():
    output_path = snakemake.output[0]
    fasta = snakemake.input[0]
    smart_trim(fasta, output_path)


if(__name__ == "__main__"):
    snakemain()
