

def parse_fastq(fpath):
    with open(fpath) as f:
        for entry in zip(f, f, f, f):
            yield entry


def write_fastq(entries, outpath):
    with open(outpath, "w") as fout:
        for e in entries:
            for line in e:
                fout.write(line)


def double_seq(entry):
    """ duplicates and appends a sequence to itself for some fastq entry
    """
    l1, l2, l3, l4 = entry
    l2 = l2.rstrip() + l2
    l4 = l4.rstrip() + l4
    return (l1, l2, l3, l4)


def main(fastq_path, output_path):
    """ Creates a sequence duplicated version of the input fastq
    """
    fq_iter = parse_fastq(fastq_path)
    doubled = (double_seq(e) for e in fq_iter)
    write_fastq(doubled, output_path)


def snakemain():
    output_path = snakemake.output[0]
    fastq = snakemake.input[0]
    main(fastq, output_path)


if(__name__ == "__main__"):
    snakemain()
