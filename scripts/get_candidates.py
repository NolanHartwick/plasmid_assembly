import time
import pandas
from Bio import SeqIO
import json

from trim_polished import write_fasta


def cluster_reads(paf, frac_diff):
    """ creates a graph representing read linkage from a paf file
    """
    clusters = {}
    cols = "qname qlen qstart qend strand tname tlen tstart tend matches length qual"
    cols = cols.split()
    chunks = pandas.read_csv(paf, header=None, sep="\t", chunksize=10**6)
    for i, chunk in enumerate(chunks):
        chunk = chunk.iloc[:, :len(cols)]
        chunk.columns = cols
        # filter self_matches
        chunk = chunk[chunk.qname != chunk.tname]
        # filter fractional matches - alignment must be within 5 percent of total read length
        chunk = chunk[(chunk.length / chunk.qlen * 2  - 1).abs() < frac_diff]
        chunk = chunk[(chunk.length / chunk.tlen - 1).abs() < frac_diff]
        # merge clusters
        for qn, tn in zip(chunk.qname, chunk.tname):
            c1 = clusters.get(qn, {qn})
            c2 = clusters.get(tn, {tn})
            if(c1 is not c2):
                merged = c1 | c2
                for r in merged:
                    clusters[r] = merged
                    clusters[r] = merged
    filtered = {tuple(sorted(c)) for c in clusters.values()}
    return filtered

    with open("clusters.json", "w") as fout:
        json.dump(list(filtered), fout, indent=4)
    filtered = {c for c in filtered if(len(c) > min_size)}
    return filtered


def gen_length_dict(fpath):
    """ given a fastq file, return a dictionary mapping from read ids to read
        lengths
    """
    seqlendict = {}
    seqlendict = {
        record.id: len(record.seq) for record in SeqIO.parse(fpath, "fastq")
    }
    return seqlendict


def get_reads(fpath, read_ids):
    """ extract the sequence for read_ids from the fastq at fpath
    """
    ret = {}
    for record in SeqIO.parse(fpath, "fastq"):
        if(record.id in read_ids):
            ret[record.id] = str(record.seq)
    return ret


def main(fastq, paf, fa_out, clst_out, len_out, info_out, frac_diff=0.05, min_size=5):
    """ Reads a paf file, creates a graph representing complete reads,
        and writes duplicated candidate sequences to output_path 
    """
    print(f"Clustering {fastq} using {paf} with frac_diff={frac_diff} and min_size={min_size}")
    clusters = cluster_reads(paf, frac_diff)
    with open(clst_out, "w") as fout:
        json.dump(list(clusters), fout, indent=4)
    filtered = {c for c in clusters if(len(c) > min_size)}
    lengths = gen_length_dict(fastq)
    with open(len_out, "w") as fout:
        json.dump(lengths, fout, indent=4)
    candidates = []
    for c in filtered:
        sort_c = list(sorted(c, key=lambda r: lengths[r]))
        med = len(sort_c) // 2
        candidates.append(sort_c[med])
    info = {
        candidate: {"cluster_size": len(c), "sequence_length": lengths[candidate]}
        for candidate, c in zip(candidates, filtered)
    }
    with open(info_out, "w") as fout:
        json.dump(info, fout, indent=4)
    sequences = get_reads(fastq, set(candidates))
    sequences = {"read_" + r: s + s for r, s in sequences.items()}
    with open(fa_out, "w") as fout:
        write_fasta(sequences, fout)


def snakemain():
    fa_out, clst_out, len_out, info_out = snakemake.output
    fastq = snakemake.input[0]
    paf = snakemake.input[1]
    frac_diff = snakemake.params["frac_diff"]
    min_size = snakemake.params["min_size"]
    main(fastq, paf, fa_out, clst_out, len_out, info_out, frac_diff, min_size)


if(__name__ == "__main__"):
    snakemain()
