import pandas
import json
import seaborn
from matplotlib import pyplot
import os


def load_json(f):
    with open(f) as fin:
        return json.load(fin)


def load_paf(f):
    cols = "qname qlen qstart qend strand tname tlen tstart tend matches length qual"
    cols = cols.split()
    df = pandas.read_csv(f, header=None, sep="\t")
    df = df.iloc[:, :len(cols)]
    df.columns = cols
    return df


def alignment_to_histdata(df):
    return pandas.Series({
        "starts": list(df.tstart),
        "ends": list(df.tend),
        "tlen": df.tlen.iloc[0],
        "tname": df.tname.iloc[0]
    })


def stacked_hist(s):
    """ given a series with entries (starts, ends, tlen, tname, outpath),
        create a stacked histogram showing the starts and ends
    """
    total = s.starts + s.ends
    total.append(s.tlen)
    total.append(0)
    pyplot.figure()
    seaborn.distplot(total, bins=50, color="red", kde=False) 
    seaborn.distplot(s.starts, color="blue", kde=False)
    pyplot.title("")
    pyplot.savefig(s.outpath)
    return True


def coverage_plot(s):
    pyplot.figure()
    coverage = [0 for i in range(s.tlen)]
    for st, en in zip(s.starts, s.ends):
        for i in range(st, en):
            coverage[i] += 1
    seaborn.lineplot(x=range(s.tlen), y=coverage)
    pyplot.savefig(s.outpath)
    return True


def main(paf_path, clst_json, outdir):
    clusters = load_json(clst_json)
    mapping = {s: i for i, c in enumerate(clusters) for s in c}
    alignments = load_paf(paf_path)
    tclusters = alignments.tname.str.split("_").str[1].apply(mapping.get)
    qclusters = alignments.qname.apply(mapping.get)
    alignments = alignments[tclusters == qclusters]
    hist_sets = alignments.groupby("tname").apply(alignment_to_histdata)
    hist_sets["outpath"] = outdir + "/" + hist_sets.tname
    os.makedirs(outdir, exist_ok=True)
    hist_sets.apply(coverage_plot, axis=1)


def snakemain():
    paf_in, clst_json = snakemake.input
    outdir = snakemake.output[0]
    main(paf_in, clst_json, outdir)


if(__name__ == "__main__"):
    snakemain()