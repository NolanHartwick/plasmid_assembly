# plasmid_assembly

Assemble/polish circular plasmids based on full length ONT reads

## setup/installation/execution

You will need to clone the repo and setup the conda environment defined in "requirements.yml". Example commands that should do the trick:

```
git clone https://gitlab.com/NolanHartwick/plasmid_assembly.git
cd plasmid_assembly
conda env create -f requirements.yml -n your_env_name
``` 

Once these finish, fo grab yourself some ONT fastq formatted reads from a plasmid population and you are ready to run. You will first activate the environment then invoke snakemake to run the workflow. Commands should look something like:

```
conda activate your_env_name
snakemake -j 4 -d /path/to/your/out_dir --config reads=/path/to/your/fastq min_size=100
```


## Algorithm

Basically, the pipeline involves running the following steps

1. duplicate the reads so that we get good alignments regardless of where the circular plasmid read happened to start
2. align the reads against the duplicated reads
3. group reads into matching "clusters" based on the alignments
4. choose a median length read from each sufficiently large cluster to act as a candidate plasmid sequence
5. align the reads against the candidate sequences and polish with racon (repeat two more times)
6. trim polished sequences so that there is no overlap at the ends as a result of circularity

In addition to supplying reads to the pipeline, you may need to tweak the clustering parameters: min_size and frac_diff. 'frac_diff' is a setting that controls how similar two reads need to be for their clusters to be merged. A smaller value means reads need to be more similar to cluster together meaning you should see more smaller clusters. The default is '0.01'. 'min_size' controls how many reads need to cluster together in order for a candidate to be selected from it. Depending on your expected read depth for your plasmids of interest, this value may need to be tuned to prevent incomplete reads that are similar by chance producing a candidate sequence. The default is '100' meaning that plasmids in your sample that didn't get 100 full length reads during sequencing won't show up in your final fasta. I'd reccomend setting this value to be roughly the square root of your expected sequencing depth for the sample you are sequencing.
